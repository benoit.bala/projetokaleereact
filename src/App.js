import {BrowserRouter, Routes, Route} from "react-router-dom";
import Contact from "./pages/Contact";
import Home from "./pages/Home";
import Notfound from "./pages/Notfound";
import Tarifs from "./pages/Tarifs";


function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/fonctionnalités" element={ <Home /> } />
        <Route path="/contact" element={ <Contact /> } />
        <Route path="/*" element={ <Notfound /> } />
        <Route path="/tarifs" element={ <Tarifs /> } />

      </Routes>
    </BrowserRouter>
  );
}

export default App;

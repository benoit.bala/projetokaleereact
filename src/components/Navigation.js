import React from 'react';
import {NavLink} from 'react-router-dom';

const Navigation = () => {
    return (
        <nav>
            <div class="navbar">
                <NavLink to="/fonctionnalités">
                <button >Fonctionnalités</button> 
                </NavLink>
                <NavLink to="/tarifs">
                <button >Tarifs</button> 
                </NavLink>
                <NavLink to="/contact">
                <button >Contact</button> 
                </NavLink>
                <NavLink to="/*">
                <button >Connection</button>
                </NavLink>
                
            </div>
            
        </nav>
    );
};

export default Navigation;
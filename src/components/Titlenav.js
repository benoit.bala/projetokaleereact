import React from 'react';
import Logo from './Logo';

const Titlenav = () => {
    return (
        <div>
            <div class='conteneurTitle'>

                <Logo />
                <h1 class="title1">Okalee</h1>
            </div>
        </div>
    );
};

export default Titlenav;
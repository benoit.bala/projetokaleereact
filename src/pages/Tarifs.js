import React from 'react';

import Navigation from '../components/Navigation';
import Titlenav from '../components/Titlenav';

const Tarifs = () => {
    return (
        <div>
            <Titlenav />
            <Navigation />
        </div>
    );
};

export default Tarifs;
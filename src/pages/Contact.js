import React from 'react';

import Navigation from '../components/Navigation';
import Titlenav from '../components/Titlenav';

const Contact = () => {
    return (
        <div>
            <Titlenav />
            <Navigation />
            
        </div>
    );
};

export default Contact;